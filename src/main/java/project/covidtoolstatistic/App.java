package project.covidtoolstatistic;

import javafx.application.Application;
import javafx.stage.Stage;
import project.covidtoolstatistic.ui.pages.Dashboard;

public class App extends Application {
  @Override
  public void start(Stage primaryStage) {
    new Dashboard().showComponent();
  }

  public static void main(String[] args) {
    launch(args);
  }
}
