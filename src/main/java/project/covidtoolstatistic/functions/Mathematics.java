package project.covidtoolstatistic.functions;

import org.math.plot.Plot2DPanel;
import javax.swing.*;
import java.awt.*;

public class Mathematics {
  private final double persons;
  private final double infections;
  private final double days;
  private final double constantK;
  private final double[] arrayDays;
  private final double[] arrayInfections;
  private final JFrame pane;

  public Mathematics(int persons, int infections, int days) {
    this.persons = persons;
    this.infections = infections;
    this.days = days;
    this.constantK = getConstant();
    this.arrayDays = getDays();
    this.arrayInfections = getInfections();
    this.pane = new JFrame("Dias - Contagios");
  }

  public double getConstant() {
    return Math.log(((persons/infections) - 1)/(persons - 1))/ - (persons * days);
  }

  public int getNumberInfections(double newDays) {
    return (int) (persons/(1 + ((persons - 1) * Math.pow(Math.E, - (persons * newDays * constantK)))));
  }

  public double[] getDays() {
    double[] arrayDays = new double[10];
    int count = 0;
    for (int i = (int) (days + 1); i < days + 11; i++) {
      arrayDays[count++] = i;
    }
    return arrayDays;
  }

  public double[] getInfections() {
    double[] arrayInfections = new double[10];
    for (int i = 0; i < arrayDays.length; i++) {
      arrayInfections[i] = getNumberInfections(arrayDays[i]);
    }
    return arrayInfections;
  }

  public double[] getAllDays() {
    return this.arrayDays;
  }

  public double[] getAllInfections() {
    return this.arrayInfections;
  }

  public void graphicFunction() {
    Plot2DPanel plot = new Plot2DPanel();
    plot.addLinePlot("Covid-Tool-Statistic", this.getAllDays(), this.getAllInfections());
    Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
    this.pane.pack();
    this.pane.setSize(dim.width/2, dim.height/2);
    this.pane.setLocationRelativeTo(null);
    this.pane.setContentPane(plot);
    this.pane.setVisible(true);
  }

  public void closePane() {
    this.pane.setVisible(false);
  }

  public String getDanger() {
    double numberInfections = this.getAllInfections()[this.getAllInfections().length - 1];
    if (numberInfections <= (this.persons * 0.1)) {
      return "BAJO";
    } else if (numberInfections > 10 && numberInfections <= (this.persons * 0.2)) {
      return "MEDIO";
    } else {
      return "ALTO";
    }
  }
}
