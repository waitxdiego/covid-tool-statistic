package project.covidtoolstatistic.ui.pages;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import project.covidtoolstatistic.functions.Mathematics;
import project.covidtoolstatistic.ui.atoms.AppButton;
import project.covidtoolstatistic.ui.atoms.ButtonImage;
import project.covidtoolstatistic.ui.molecules.AppOutputText;
import project.covidtoolstatistic.ui.molecules.AppTextField;
import project.covidtoolstatistic.ui.organisms.EmergentWindow;

public class Dashboard {
  private final Stage stage;
  private final VBox root;
  private final ButtonImage exit;
  private final HBox topBar;
  private final GridPane pane;
  private final AppTextField population;
  private final AppTextField infections;
  private final AppTextField days;
  private final AppButton reset;
  private final HBox generalBody;
  private final AppButton calculate;
  private final AppOutputText danger;
  private final AppOutputText variant;
  private final HBox footer;
  private Integer numberPopulation;
  private Integer numberInfections;
  private Integer numberDays;
  private Mathematics graphicFunction;

  public Dashboard() {
    this.stage = new Stage();
    this.root = new VBox();
    this.exit = new ButtonImage("exit-button.png", 30, 30);
    this.topBar = new HBox(this.exit.getComponent());
    this.pane = new GridPane();
    this.population = new AppTextField(
            "POBLACION: ", 108, 243);
    this.infections = new AppTextField(
            "Contagios: ", 108, 243);
    this.days = new AppTextField(
            "Dias: ", 108, 243);
    VBox rightBody = new VBox(70,
            this.population.getComponent(),
            this.infections.getComponent(),
            this.days.getComponent()
    );
    rightBody.setAlignment(Pos.CENTER);
    this.reset = new AppButton("Reset", "#727473");
    this.calculate = new AppButton("CALCULAR", "#14ae5c");
    VBox leftBody = new VBox(75,
            this.reset.getComponent(),
            this.calculate.getComponent()
    );
    leftBody.setAlignment(Pos.CENTER);
    this.generalBody = new HBox(75, rightBody, leftBody);
    this.danger = new AppOutputText("RIESGO: ");
    this.variant = new AppOutputText("POSIBILIDAD DE VARIANTE CODVID-19: ");
    this.footer = new HBox(60,
            this.danger.getComponent(),
            this.variant.getComponent()
    );
    this.numberPopulation = null;
    this.numberInfections = null;
    this.numberDays = null;
  }

  private Node getComponent() {
    return this.root;
  }

  private void configureActions() {
    this.exit.getButton().setOnAction(e -> exitClicked());
    this.reset.getButton().setOnAction(e -> resetClicked());
    this.calculate.getButton().setOnAction(e -> calculateClicked());
  }

  private void configureComponent() {
    this.footer.setAlignment(Pos.CENTER_LEFT);
    this.topBar.setPrefWidth(1020);
    this.topBar.setPrefHeight(55);
    this.topBar.getStyleClass().add("top");
    this.topBar.setAlignment(Pos.CENTER_RIGHT);
    this.danger.getComponent().setAlignment(Pos.CENTER);
    this.variant.getComponent().setAlignment(Pos.CENTER);
    this.pane.setPadding(new Insets(45));
    this.pane.setVgap(70);
    this.pane.setAlignment(Pos.CENTER);
    this.pane.getStyleClass().add("background");
    this.root.setAlignment(Pos.CENTER);
  }

  private void composeComponent() {
    this.pane.addRow(0, this.generalBody);
    this.pane.addRow(1, this.footer);
    this.root.getChildren().addAll(this.topBar, this.pane);
  }

  private Node build() {
    this.configureActions();
    this.configureComponent();
    this.composeComponent();
    return getComponent();
  }

  public void showComponent() {
    Scene scene = new Scene((Parent) this.build(), 680, 541);
    scene.getStylesheets().add("/styles.css");
    this.stage.setScene(scene);
    this.stage.initStyle(StageStyle.UNDECORATED);
    this.stage.show();
  }

  private void exitClicked() {
    this.stage.close();
    try {
      this.graphicFunction.closePane();
    } catch (NullPointerException ignored) {}
    System.exit(0);
  }

  private void resetClicked() {
    this.stage.close();
    new Dashboard().showComponent();
  }

  private void calculateClicked() {
    try {
      this.numberPopulation = Integer.parseInt(String.valueOf(this.population.getTextField().getCharacters()));
      if (this.numberPopulation > 0){
        this.population.getTextField().setStyle("-fx-background-color: #4cb34c;");
      } else {
        this.numberPopulation = null;
        this.population.getTextField().setStyle("-fx-background-color: #d34b4b;");
        new EmergentWindow("El número de poblacion debe ser mayor a 0").showComponent();
      }
    } catch (NumberFormatException e) {
      this.population.getTextField().setStyle("-fx-background-color: #d34b4b;");
      new EmergentWindow("El número de poblacion debe ser un numero Entero Positivo").showComponent();
    }
    try {
      this.numberInfections = Integer.parseInt(String.valueOf(this.infections.getTextField().getCharacters()));
      if (this.numberInfections > 0){
        this.infections.getTextField().setStyle("-fx-background-color: #4cb34c;");
      } else {
        this.numberInfections = null;
        this.infections.getTextField().setStyle("-fx-background-color: #d34b4b;");
        new EmergentWindow("El número de contagios debe ser mayor a 0").showComponent();
      }
    } catch (NumberFormatException e) {
      this.infections.getTextField().setStyle("-fx-background-color: #d34b4b;");
      new EmergentWindow("El número de contagios debe ser un número Entero Positivo").showComponent();
    }
    try {
      this.numberDays = Integer.parseInt(String.valueOf(this.days.getTextField().getCharacters()));
      if (this.numberDays > 0){
        this.days.getTextField().setStyle("-fx-background-color: #4cb34c;");
      } else {
        this.numberDays = null;
        this.days.getTextField().setStyle("-fx-background-color: #d34b4b;");
        new EmergentWindow("El número de dias debe ser mayor a 0").showComponent();
      }
    } catch (NumberFormatException e) {
      this.days.getTextField().setStyle("-fx-background-color: #d34b4b;");
      new EmergentWindow("El número de dias debe ser un número Entero Positivo").showComponent();
    }
    if (this.numberPopulation != null && this.numberInfections != null && this.numberDays != null) {
      this.graphicFunction();
    }
  }

  private void graphicFunction() {
    this.graphicFunction = new Mathematics(this.numberPopulation, this.numberInfections, this.numberDays);
    this.graphicFunction.graphicFunction();
    if (this.graphicFunction.getDanger().equals("BAJO")) {
      this.danger.getTextField().setStyle("-fx-text-fill: #1c4a1c;");
      this.danger.getTextField().setText("BAJO");
      this.variant.getTextField().setStyle("-fx-text-fill: #1c4a1c;");
      this.variant.getTextField().setText("NINGUNA");
    } else if (this.graphicFunction.getDanger().equals("MEDIO")) {
      this.danger.getTextField().setStyle("-fx-text-fill: #82823b;");
      this.danger.getTextField().setText("MEDIO");
      this.variant.getTextField().setStyle("-fx-text-fill: #82823b;");
      this.variant.getTextField().setText("OMICRON");
    } else {
      this.danger.getTextField().setStyle("-fx-text-fill: #9d0606;");
      this.danger.getTextField().setText("ALTO");
      this.variant.getTextField().setStyle("-fx-text-fill: #9d0606;");
      this.variant.getTextField().setText("DELTA");
    }
  }
}
