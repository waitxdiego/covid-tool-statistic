package project.covidtoolstatistic.ui.molecules;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

public class AppOutputText {
  private final Label space;
  private final HBox field;

  public AppOutputText(String getTitle) {
    Label title = new Label(getTitle);
    this.space = new Label();
    this.field = new HBox(5, title, this.space);
    this.space.getStyleClass().add("space");
    title.getStyleClass().add("space");
  }

  public Label getTextField() {
    return this.space;
  }

  private void configureComponent() {
    this.field.setAlignment(Pos.CENTER_LEFT);
    this.field.setPadding(new Insets(5));
    this.space.setStyle("-fx-background-color: transparent");
    this.space.setMinHeight(25);
    this.space.setMaxWidth(125);
  }

  public HBox getComponent() {
    this.configureComponent();
    return this.field;
  }
}
