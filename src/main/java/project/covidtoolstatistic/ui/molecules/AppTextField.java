package project.covidtoolstatistic.ui.molecules;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

public class AppTextField {
  private final TextField space;
  private final HBox field;
  private final int width;
  private final int boxWidth;

  public AppTextField(String getTitle, int width, int boxWidth) {
    Label title = new Label(getTitle);
    this.space = new TextField();
    this.field = new HBox(5, title, this.space);
    this.width = width;
    this.boxWidth = boxWidth;
  }

  public TextField getTextField() {
    return this.space;
  }

  private void configureComponent() {
    this.space.setAlignment(Pos.CENTER);
    this.space.setPrefWidth(this.width);
    this.space.setPrefHeight(37);
    this.field.setAlignment(Pos.CENTER_RIGHT);
    this.field.setPadding(new Insets(5));
    this.field.setStyle("-fx-background-color: #f8f8f8;");
    this.field.setMinHeight(55);
    this.field.setMaxWidth(this.boxWidth);
    this.space.setStyle("-fx-background-color: #b3b3b3;");
  }

  public HBox getComponent() {
    this.configureComponent();
    return this.field;
  }
}
