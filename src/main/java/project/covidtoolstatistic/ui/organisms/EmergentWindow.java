package project.covidtoolstatistic.ui.organisms;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class EmergentWindow {
  private final Stage stage;
  private final GridPane pane;
  private final ImageView icon;
  private final HBox body;
  private final Button ok;
  private final VBox generalBox;

  public EmergentWindow(String message) {
    this.stage = new Stage();
    this.pane = new GridPane();
    Label title = new Label(message);
    this.icon = new ImageView(new Image("/sign-error-icon.png"));
    this.body = new HBox(15, icon, title);
    this.ok = new Button("Ok");
    this.generalBox = new VBox(25, body, this.ok);
  }

  private Node getComponent() {
    return this.pane;
  }

  private void configureActions() {
    this.ok.setOnAction(e -> okClicked());
  }

  private void configureComponent() {
    this.pane.setAlignment(Pos.CENTER);
    this.generalBox.setAlignment(Pos.CENTER);
    this.generalBox.setPadding(new Insets(15));
    this.body.setAlignment(Pos.CENTER);
    this.icon.setFitHeight(50);
    this.icon.setFitWidth(50);
  }

  private void composeComponent() {
    this.pane.addRow(0, this.generalBox);
  }

  private Node build() {
    this.configureActions();
    this.configureComponent();
    this.composeComponent();
    return this.getComponent();
  }

  public void showComponent() {
    this.stage.setTitle("ERROR!!");
    this.stage.setScene(new Scene((Parent) this.build(), 500, 150));
    this.stage.show();
  }

  private void okClicked() {
    this.stage.close();
  }

}
