package project.covidtoolstatistic.ui.atoms;

import javafx.scene.Cursor;
import javafx.scene.control.Button;

public class AppButton {
  private final Button button;
  private final String color;

  public AppButton(String text, String color) {
    this.button = new Button(text);
    this.color = color;
  }

  public Button getButton() {
    return this.button;
  }

  private void configureComponent() {
    this.button.setStyle("-fx-background-color: " + color + ";");
    this.button.getStyleClass().add("appButton");
    this.button.setCursor(Cursor.HAND);
  }

  public Button getComponent() {
    this.configureComponent();
    return getButton();
  }
}
