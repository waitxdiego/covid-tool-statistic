package project.covidtoolstatistic.ui.atoms;

import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ButtonImage {
  private final ImageView image;
  private final Button button;
  private final int sizeX;
  private final int sizeY;

  public ButtonImage(String url, int sizeX, int sizeY) {
    this.image = new ImageView(new Image("/" + url));
    this.button = new Button();
    this.sizeX = sizeX;
    this.sizeY = sizeY;
  }

  public Button getButton() {
    return this.button;
  }

  public Button getComponent() {
    this.configureComponent();
    return getButton();
  }

  private void configureComponent() {
    this.image.setFitWidth(this.sizeX);
    this.image.setFitHeight(this.sizeY);
    this.button.setGraphic(this.image);
    this.button.setAlignment(Pos.CENTER_RIGHT);
    this.button.setCursor(Cursor.HAND);
    this.button.getStyleClass().add("buttonImage");
  }
}
