module project {
  requires javafx.controls;
  requires JMathPlot;
  requires java.desktop;
  requires matplotlib4j;
  requires slf4j.api;

  exports project.covidtoolstatistic;
  exports project.covidtoolstatistic.functions;
  exports project.covidtoolstatistic.ui.pages;
  exports project.covidtoolstatistic.ui.organisms;
  exports project.covidtoolstatistic.ui.molecules;
  exports project.covidtoolstatistic.ui.atoms;
}